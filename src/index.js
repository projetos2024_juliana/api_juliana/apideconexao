const express = require('express')
const app = express()
const testConnect = require('./db/testConnect')

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
      testConnect();
    }

    middlewares() {
      this.express.use(express.json());
    }

    routes() {
      const rotas = require("./routes/rotas")
      this.express.use('/api/v1/', rotas);
    
    }
  }

  module.exports = new AppController().express;
