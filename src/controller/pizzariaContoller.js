const connect = require("../db/connect");

module.exports = class pizzariaController {
  static async listarPedidosPizzas(req, res) {
    const query =  `
    SELECT
      pp.fk_id_pedido AS Pedido, 
      p.nome AS Pizza,
      pp.quantidade AS Qtde, 
      ROUND((pp.valor / pp.quantidade), 2) AS Unitário,
      pp.valor AS Total
    FROM
      pizza_pedido pp,
      pizza p
    WHERE
      pp.fk_id_pizza = p.id_pizza
    ORDER BY
      pp.fk_id_pedido;
  `;

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          return res.status(500).json({ error: "Erro ao consultar pedidos de pizzas no banco de dados!!!" });
        }  // Fim do if

        console.log("Consulta de pedidos de pizzas realizada com sucesso!!!");
        return res.status(200).json({ result });
      });  // Fim do connect.query
    }  // Fim do try
    
    catch (error) {
      console.error("Erro ao executar a consulta de pedidos de pizzas: ", error);
      return res.status(500).json({ error: "Erro interno do servidor!!!" });

    } // Fim do catch

  }  // Fim do listarPedidosPizzas

  static async  listarPedidosPizzasComJoin(req, res) {
    const query = `select
      pp.fk_id_pedido as pedido,
      p.nome as pizza,
      pp.quantidade as qtde,
      round((pp.valor / pp.quantidade), 2) as unitário,
      pp.valor as total
  from
      pizza_pedido pp
  left join
      pizza p on pp.fk_id_pizza = p.id_pizza

  union

  select
      pp.fk_id_pedido as pedido,
      p.nome as pizza,
      pp.quantidade as qtde,
      round((pp.valor / pp.quantidade), 2) as unitário,
      pp.valor as total
  from
      pizza_pedido pp
  right join
      pizza p on pp.fk_id_pizza = p.id_pizza
  where
      pp.fk_id_pedido is null
     order by 
         pedido;
        
        `; //Fim da atribuição da constante

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          return res.status(500).json({ error: "Erro ao consultar o banco de dados!!!" });
        }  // Fimm do catch

        console.log("Consulta realizada com sucesso!!!");
        return res.status(200).json({ result });
      }); //Fim da connect.query

    } catch (error) {
      console.error("Erro ao executar a consulta de pedidos de pizzas: ", error );  // Fim do console
      return res.status(500).json({ error: "Erro interno do servidor!!!" });
    }  // Fim do catch
  }  // Fim da async istarPedidosPizzasComJoin
}; //  Fim do module exports