const router = require('express').Router();
// const dbController = require('../controller/dbController')
// const clienteController = require("../controller/clienteController");
const pizzariaController = require("../controller/pizzariaContoller");


// // //rota para consulta das tabelas do banco
// router.get("/tables/", dbController.getTables);
// router.get("/tablesDescription/", dbController.getTablesDescription);

// // rota para cadastro de clientes
// router.post("/postcliente/", clienteController.createCliente);

// // rota para select da tabela cliente
// router.get("/clientes/", clienteController.getAllClientes);

// // rota para select da tabela cliente com filtro 
// router.get("/clientes2/", clienteController.getAllClientes2);

// // rota para select da tabela cliente ordenacao
// router.get("/procurarCliente/", clienteController.getAllClientes2);

// rota para listar as pizzas pedidas
// router.get("/listarPedidosPizza/", pizzariaController.listarPedidosPizzas);

// rota para listar pedidos com JOIN
router.get("/listarPedidosPizzasComJoin/",pizzariaController.listarPedidosPizzasComJoin);

module.exports = router;
